var fileInfo = $('#upload-file-info');
var uploadButton = $('#file-selector');
var loader = $('.loader');
var form = $('#form');
var img = $('#file-img');

var title = $('#form_title');
var composer = $('#form_composer');
var musicType = $('#form_musicType');
var description = $('#form_description');
var duration = $('#form_duration');
var photo = $('#form_photo');
var filename = $('#form_filename');
var downloadable = $('#form_downloadable');
var explicitContent = $('#form_explicit_content');
var visible = $('#form_visible');

requestTrackTypes();

uploadButton.change(function() {
    if(!uploadButton.hasClass('disabled')) {
        img.css('display', 'none');
        readMetadata(uploadButton[0].files[0]);
    }
});

form.submit(function() {
    $.ajax({
        url: 'http://localhost:8001/track',
        crossDomain: true,
        method: 'POST',
        data: {
            title: title.val(),
            composer: composer.val(),
            musicType: musicType.val(),
            description: description.val(),
            duration: duration.val(),
            photo: photo.val(),
            filename: filename.val(),
            downloadable: downloadable.is(':checked'),
            explicitContent: explicitContent.is(':checked'),
            visible: visible.is(':checked'),
            user: 4
        },
        success: function(data, status, xhr) {
            console.log(data);
        }
    });

    return false;
});


function requestTrackTypes() {
    $.ajax({
        url: 'http://localhost:8001/tracks/types',
        method: 'GET',
        crossDomain: true,
        success: function(data) {

            data.content.types.forEach(function(element) {
                $('#form_musicType').append($('<option>', {
                    value: element.id,
                    text: element.label
                }));
            });

        },
        error: function(error) {
            console.log("Error while getting track types...");
        }
    });
}

function readMetadata(file) {
    fileInfo.html(file.name);
    uploadButton.addClass('disabled');
    loader.css('display', 'block');

    var formData = new FormData();
    formData.append('filename', file);

    $.ajax({
        url: 'http://localhost:8001/track/metadata',
        crossDomain: true,
        data: formData,
        processData: false,
        contentType: false,
        method: 'POST',
        success: function(data) {
            uploadButton.removeClass('disabled');
            loader.css('display', 'none');
            form.css('display', 'block');

            var base64Picture = data.content.metadata.photo;
            if(base64Picture != null) {

                img.attr('src', 'data:image/png;base64,' + base64Picture);
                img.css('display', 'inline-block');
            }

            fillForm(data.content.metadata);
        },
        error: function(error) {
            console.log(error);
            uploadButton.removeClass('disabled');
            loader.css('display', 'none');
        }
    });
}

function fillForm(data) {
    title.val(data.title);
    composer.val(data.composer);
    description.val(data.description);
    duration.val(data.duration);
    photo.val(data.photo);
    filename.val(data.filename);

    musicType.val(getIndexOfType(data.musicType));
}

function getIndexOfType(musicType) {
    var musicTypes = document.getElementById('form_musicType').options;

    for(var i = 0; i < musicTypes.length; i++) {
        if(musicTypes[i].label === musicType) {
            return musicType[i].value;
        }
    }

    return 0;
}
