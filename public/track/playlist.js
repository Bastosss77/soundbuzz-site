var currentTracks = [];
var currentIndex = 0;
var audioSrc = $('#audioSource');
var composer = $('#composer');
var title = $('#title');
var audioPlay = $("#play_btn");
var audioPause = $("#pause_btn");

$('#playButton').click(function(event) {
    var id = $(this).parent().parent().attr('id');

    requestTrackForPlaylist(id).then(function(val){
        currentTracks = val;
        loadTrack(currentTracks[0]);
    }).catch(function() {
        console.error("error");
    });
});

$('#prev_btn').click(function() {
    calculatePreviousIndex();
    loadTrack(currentTracks[currentIndex]);
});

$('#next_btn').click(function() {
   calculateNextIndex();
   loadTrack(currentTracks[currentIndex]);
});


$('#control-random').click(function() {

    currentIndex = getRandomInt(currentTracks.length);
    loadTrack(currentTracks[currentIndex]);
});

$('#audio').on('ended', function() {
    calculateNextIndex();
    loadTrack(currentTracks[currentIndex]);
});

function requestTrackForPlaylist(playlistId) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            url: 'http://localhost:8001/playlist/' + playlistId + '/tracks',
            method: 'GET',
            crossDomain: true,
            success: function(data, textStatus, xhr) {
                if(xhr.status === 200) {
                    resolve(data['content']['tracks']);
                } else {
                    reject("Error");
                }
            }
        });
    });
}

function loadTrack(track) {
    audioPlay.show();
    audioPause.hide();

    composer.html(track.composer);
    title.html(track.title);

    $.ajax({
        url:  "http://localhost:8001/tracks/play?name=" + track.filename,
        method: "GET",
        crossDomain: true,
        success: function(data) {
            audioSrc.attr("src", data);
            audio.load();
            audio.play();

            audioPlay.hide();
            audioPause.show();
        },
        error: function(err) {
            console.error(err);
        }
    });
}

function calculatePreviousIndex() {
    currentIndex = currentIndex === 0 ? currentTracks.length - 1 : currentIndex - 1;
}

function calculateNextIndex() {
    currentIndex = currentIndex === currentTracks.length - 1 ? 0 : currentIndex + 1;
}