$('.approveButton, .refuseButton, .cancelButton, .deleteButton').click(function(){
    var id = $(this).attr("aria-value");
    var methode=$(this).hasClass('btn-success')? 'PUT' : 'DELETE';
    var data=$(this).hasClass('btn-success') ? $(this).hasClass('cancelButton') ? {signaled : 0} : { visible: 1 } : {};
    var btn = $(this);
    $('.btn').addClass('disabled');

    $.ajax({
        url: 'http://localhost:8001/track/' + id,
        method: methode,
        data:data,
        crossDomain: true,
        success: function(data) {
            $('.btn').removeClass('disabled');
            btn.closest('tr').remove();
        },
        error: function(err){
            console.log(err);
            $('.btn').removeClass('disabled');
        }
    });
});