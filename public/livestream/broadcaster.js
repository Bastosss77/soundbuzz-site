(function () {
    'use strict';

    ws.onopen = function () {
        ws.send(JSON.stringify({
            action: 'server_create',
            liveId: livestreamId,
            userId: userId
        }));
    };

    ws.onmessage = function (event) {
        var data = JSON.parse(event.data);

        if(data.action === "connected") {
            _receiver.innerHTML = 'Connected as n°' + data.data;
        } else if(data.action === "client_candidate") {
            if(data.data != null) {
                serverConnection.addIceCandidate(new RTCIceCandidate(data.data), function () {
                    console.log('add ice candidate success');
                }, function (err) {
                    console.error(err);
                });
            }
        } else if(data.action === 'client_description') {
            serverConnection.setRemoteDescription(new RTCSessionDescription(data.data));
        }
    };

    ws.onclose = function () {
        ws.send(JSON.stringify({
            action: 'server_close',
            liveId: livestreamId
        }));

        close();
    };

    ws.onerror = function () {
        ws.send(JSON.stringify({
            action: 'server_close',
            liveId: livestreamId
        }));

        close();
    };
})();


navigator.mediaDevices.getUserMedia({ audio: true, video: false })
    .then(function(stream) {
        onMediaUserSuccess(stream);
    }).catch(function(error) {
    alert("Vous devez autoriser l'accès au microphone pour pouvoir lancer le live")
});

function onMediaUserSuccess(stream) {
    var servers = [];
    var pcConstraints = {
        'optional': []
    };

    if(window.mozPeerConnection) {
        serverConnection = new mozRTCPeerConnection(servers, pcConstraints)
    } else if(window.webkitPeerConnection) {
        serverConnection = new webkitRTCPeerConnection(servers, pcConstraints)
    } else if(window.msPeerConnection) {
        serverConnection = new msRTCPeerConnection(servers, pcConstraints)
    } else {
        serverConnection = new RTCPeerConnection(servers, pcConstraints)
    }

    serverConnection.addStream(stream);
    serverConnection.onicecandidate = sentIceCandidate;

    serverConnection.createOffer(function(desc) {
        serverConnection.setLocalDescription(desc);
        ws.send(JSON.stringify({
            action: 'server_description',
            liveId: livestreamId,
            description: desc
        }))
    }, function(error) {
        close();
    });
}

function sentIceCandidate(evt) {
    if(evt.candidate) {
        ws.send(JSON.stringify({
            action: 'server_candidate',
            liveId: livestreamId,
            candidate: evt.candidate
        }))
    }
}

function close() {
    console.log('close');
    if(serverConnection) {
        serverConnection.close();
        ws.send(JSON.stringify({
            action: 'server_close',
            liveId: livestreamId
        }));

        serverConnection = {};
    }
}