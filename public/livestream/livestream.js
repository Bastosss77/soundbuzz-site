$('#newStream').click(function() {
    $.ajax({
        method: 'GET',
        url: 'http://localhost:8001/livestream/new/' + userId,
        crossDomain: true,
        success: function(data, statusText, xhr) {
            if(xhr.status == 201) {
                window.location.href = 'http://localhost:8000/livestream/broadcast/' + data['content']['livestream']['streamId']
                    + '?userId=' + data['content']['livestream']['user']['id'];
            }
        },
        statusCode: {
            401: function() {
                alert('Unauthorized');
            }
        }
    });
});