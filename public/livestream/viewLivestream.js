
var ws = new WebSocket('ws://' + wsUrl);
var _receiver = document.getElementById('ws-content-receiver');
var _video = document.getElementById('remoteVideo');

var serverTempDesc = {};
var serverTempIceCandidates = [];
var clientConnection = {};

(function () {
    'use strict';

    ws.onopen = function () {
        ws.send(JSON.stringify({
            action: 'client_log',
            liveId: livestreamId,
            username: username
        }));

        ws.send(JSON.stringify({
            action: 'client_ask_description',
            liveId: livestreamId
        }));

        ws.send(JSON.stringify({
            action: 'client_ask_candidate',
            liveId: livestreamId
        }));
    };

    ws.onmessage = function (event) {
        var data = JSON.parse(event.data);

        if(data.action === "connected") {
            _receiver.innerHTML = 'Welcome user n°' + data.data;
        } else if(data.action === 'client_ask_description') {
            serverTempDesc = data.data;
        } else if(data.action === 'client_ask_candidate') {
            serverTempIceCandidates.push(data.data);
            complete();
        }
    };

    ws.onclose = function () {
    };

    ws.onerror = function () {
    };
})();

function close() {
    if(clientConnection) {
        clientConnection.close();
        ws.send(JSON.stringify({
            action: 'leave',
            liveId: livestreamId,
            username: username
        }));

        serverConnection = {};
    }
}

function complete() {
    var servers = [];
    var pcConstraints = {
        'optional': []
    };

    if(window.mozPeerConnection) {
        clientConnection = new mozRTCPeerConnection(servers, pcConstraints)
    } else if(window.webkitPeerConnection) {
        clientConnection = new webkitRTCPeerConnection(servers, pcConstraints)
    } else if(window.msPeerConnection) {
        clientConnection = new msRTCPeerConnection(servers, pcConstraints)
    } else {
        clientConnection = new RTCPeerConnection(servers, pcConstraints)
    }

    clientConnection.onicecandidate = function(event) {
        ws.send(JSON.stringify({
            action: 'client_candidate',
            liveId: livestreamId,
            candidate: event.candidate
        }));
    };

    clientConnection.setRemoteDescription(new RTCSessionDescription(serverTempDesc)).then(function() {
            clientConnection.createAnswer(gotDescription, function(error){});
        }, function(err) {
            console.error(err);
        }
    );

    clientConnection.onaddstream = function (event) {
        window.stream = event.stream;
        if (window.URL) {
            _video.src =  window.URL.createObjectURL(event.stream);
        } else {
            _video.src =  event.stream;
        }
    };
}

function gotDescription(clientDesc) {
    clientConnection.setLocalDescription(clientDesc, function() {
        registerIceCandidate();
        ws.send(JSON.stringify({
            action: 'client_description',
            liveId: livestreamId,
            description: clientDesc
        }));
    }, function(err) {
        console.error(err);
    })
}

function registerIceCandidate() {
    serverTempIceCandidates.forEach(function(val) {
        clientConnection.addIceCandidate(new RTCIceCandidate(val),
            function() {
                console.log("Success adding ice candidate");
            },
            function(err) {
                console.error(err);
            });
    });
}