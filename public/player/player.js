// var for audio content

var audio = document.getElementById('audio');
var audioSrc = $('#audioSource');

// html5 function - toggle play/pause btn and audio

$("#plays_btn").click(function() {

    if (audio.paused === false) {
        audio.pause();
        $("#play_btn").show();
        $("#pause_btn").hide();
    } else {
        audio.play();
        $("#play_btn").hide();
        $("#pause_btn").show();
    }
});

// function for timeline

audio.addEventListener("timeupdate", function() {
    var currentTime = audio.currentTime,
        duration = audio.duration,
        currentTimeMs = audio.currentTime * 1000;
    $('.progressbar_range').stop(true, true).animate({'width': (currentTime + .25) / duration * 100 + '%'}, 250, 'linear');
});


// count function for timeleft

audio.addEventListener("timeupdate", function() {
    var timeleft = document.getElementById('timeleft'),
        duration = parseInt( audio.duration ),
        currentTime = parseInt( audio.currentTime ),
        timeLeft = duration - currentTime,
        s, m;

    s = timeLeft % 60;
    m = Math.floor( timeLeft / 60 ) % 60;

    s = s < 10 ? "0"+s : s;
    m = m < 10 ? "0"+m : m;

    $('#timeleft').text("-"+m+":"+s);
});

$('#control-download').click(function() {
   var trackData = audioSrc.attr('src');
   var filename = $('#title').html() + ' - ' + $('#composer').html();

   if(trackData != null) {
        downloadTrack(trackData, filename);
    }
});

function downloadTrack(dataTrack, filename) {
    var element = document.createElement('a');
    element.setAttribute('href', dataTrack);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

//that's all folks
// sorry, others buttons dont work)