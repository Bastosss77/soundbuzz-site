$('.updateButton').click(function(){
    var id = $(this).attr("aria-value");
    var methode=$(this).hasClass('btn-success')? 'PUT' : 'DELETE';
    var data=$(this).hasClass('btn-success')? { isactif: 1 } : {};
    var currentState=$(this).hasClass('btn-success')? 'btn-success' : 'btn-danger';
    var button=$(this);
    $(this).addClass('disabled');

    $.ajax({
        url: 'http://localhost:8001/user/' + id,
        method: methode,
        data:data,

        success: function(data) {
            var newState=button.hasClass('btn-success')? 'btn-danger' :'btn-success';
            button.removeClass(currentState);
            button.addClass(newState);
            button.removeClass('disabled');
            var newValue=button.hasClass('btn-success')? 'Réactiver' : 'Désactiver';
            button.html(newValue);
        },
        error: function(err){
            button.removeClass('disabled');

        }
    });
});