<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 02/07/18
 * Time: 10:35
 */

namespace App\Controller;

use App\Form\AddTrackType;
use Symfony\Component\Routing\Annotation\Route;
use Unirest\Request as RestRequest;


class TrackController extends BaseController {

    /**
     * @Route("/tracks")
     */
    public function tracks() {
        $form = $this->createForm(AddTrackType::class);

        return $this->render('track/add.html.twig', array(
            'userId' => $this->getFromSession('userId'),
            'addTrack_form' => $form->createView()));
    }

    /**
     * @Route("/track/pending", name="track_pending")
     */
    public function trackPending(){
        $response = RestRequest::get('http://localhost:8001/tracks');
        $data = json_decode($response->raw_body, true);

        return $this->render('track/tracks_pending.html.twig',
            array('data' => $data) );
    }

    /**
     * @Route("/track/reported", name="track_reported")
     */
    public function trackReported(){
        $response = RestRequest::get('http://localhost:8001/tracks');
        $data = json_decode($response->raw_body, true);

        return $this->render('track/tracks_reported.html.twig',
            array('data' => $data) );
    }


}