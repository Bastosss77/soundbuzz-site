<?php
/**
 * Created by PhpStorm.
 * User: djamaliahamada
 * Date: 08/06/2018
 * Time: 13:19
 */

namespace App\Controller;

use App\common\Helper\UnirestHelper;
use App\Form\LostPasswordType;
use App\Form\SendLostPasswordType;
use App\Form\SignInType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Unirest\Request as RestRequest;


class UserController extends BaseController {

    /**
     * @Route("/user/manage", name="user_list")
     */
    public function userShow(){
        $response = RestRequest::get('http://localhost:8001/users');
        $data = json_decode($response->raw_body, true);

       return $this->render('user/user_managing.html.twig',
           array('data' => $data) );
    }

    /**
     * @Route("/signin", name="signin")
     */
    public function signIn(Request $request) {
        $form = $this->createForm(SignInType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid() && $request->isMethod('POST')) {
            $datas = $form->getData();
            $body = RestRequest\Body::multipart($datas);
            $response = RestRequest::post('http://localhost:8001/user', UnirestHelper::UNIREST_HEADER_JSON, $body);

            if(in_array($response->code, UnirestHelper::RESPONSE_OK)){
                $this->addFlash("success", "Votre inscription a bien été prit en compte.");

                return $this->redirectToRoute('index');
            }
            else{
                $this->addFlash("danger", "Erreur lors de l'inscription.");
                return $this->render('user/signin.html.twig', [
                    'signIn_form' => $form->createView()
                ]);
            }
        }

        return $this->render('user/signin.html.twig', [
            'signIn_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/lostpassword/", name="send_lost_password")
     */
    public function sendLostPassword(Request $request) {
        $form = $this->createForm(SendLostPasswordType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid() && $request->isMethod('POST')) {
            $datas = $form->getData();
            $body = RestRequest\Body::multipart($datas);
            $response = RestRequest::post('http://localhost:8001/user/lostpassword', UnirestHelper::UNIREST_HEADER_JSON, $body);

            if(in_array($response->code, UnirestHelper::RESPONSE_OK)){
                $this->addFlash("success", "L'email de réinitialisation a bien été envoyé.");
                return $this->redirectToRoute('index');
            }
            else{
                $this->addFlash("danger", "Erreur lors de l'envoi du mail.");
                return $this->render('user/send_lost_password.html.twig', [
                    'sendlostPwd_form' => $form->createView()
                ]);
            }
        }
        return $this->render('user/send_lost_password.html.twig', [
            'sendlostPwd_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/lostpassword/{token}", name="lost_password")
     */
    public function lostPassword($token, Request $request) {
        $form = $this->createForm(LostPasswordType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid() && $request->isMethod('POST')) {
            $datas = $form->getData();
            $body = RestRequest\Body::Multipart($datas);
            $response = RestRequest::put('http://localhost:8001/user/lostpassword/'. $token, UnirestHelper::UNIREST_HEADER_JSON, $body);
            if(in_array($response->code, UnirestHelper::RESPONSE_OK)){
                $this->addFlash("success", "Le mot de passe a bien été changé.");

                return $this->redirectToRoute('index');
            }
            else{
                $this->addFlash("danger", "Erreur lors de la modification du mot de passe.");
                return $this->render('user/lost_password.html.twig', [
                    'lostPwd_form' => $form->createView()
                ]);
            }
        }
        return $this->render('user/lost_password.html.twig', [
            'lostPwd_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user/confirm/{token}", name="confirm_registration")
     */
    public function confirmRegistrationAction($token, Request $request) {
        $response = RestRequest::get('http://localhost:8001/user/confirm/'. $token);

        if(in_array($response->code, UnirestHelper::RESPONSE_OK)){
            $this->addFlash("success", "Votre compte a bien été activé.");
            return $this->redirectToRoute('index');
        }
        else{
            $this->addFlash("danger", "Erreur lors de l'activation de votre compte. Contacter un administrateur");
            return $this->redirectToRoute('index');
        }
    }
}