<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 15/06/18
 * Time: 20:32
 */

namespace App\Controller;

use Unirest\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class PlaylistController extends BaseController {

    /**
     * @Route("/playlists", methods="GET")
     */
    public function playlists() {
        $userId = $this->getFromSession('userId');
        if($userId == null) {
            return $this->redirect('http://localhost:8001');
        }

        $response = Request::get('http://localhost:8001/playlists?userId=' . $userId);
        $playlists = json_decode($response->raw_body, true)['content']['playlists'];

        return $this->render('track/playlists.html.twig', array('playlists' => $playlists));
    }
}