<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 08/06/18
 * Time: 14:18
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Unirest\Request as RestRequest;

class LivestreamController extends BaseController {

    /** Get a list of all stream
     * @Route("/livestream", methods={"GET"})
     */
    public function livestream() {
        $response = RestRequest::get('http://localhost:8001/livestreams');
        $livestreams = json_decode($response->raw_body, true);

        return $this->render('livestream/livestream.html.twig', array(
            'livestreams' => $livestreams,
            'userId' => $this->getFromSession('userId')));
    }

    /**
     * @Route("/livestream/broadcast/{id}", methods="GET")
     */
    public function makeLivestream($id, Request $request) {
        $params = $request->query->all();

        $session = $this->get('session');
        $session->set('broadcast', array(
            'livestreamId' => $id,
            'userId' => $params['userId']
        ));

        return $this->render('livestream/brocaster.html.twig', ['ws_url' => 'localhost:7777']);
    }

    /**
     * @Route("/livestream/view/{id}", methods="GET")
     */
    public function viewLivestream($id) {
        return $this->render('livestream/view_livestream.html.twig',
            ['ws_url' => 'localhost:7777',
                'livestreamId' => $id,
                'username' => $this->getFromSession('username')]);
    }
}