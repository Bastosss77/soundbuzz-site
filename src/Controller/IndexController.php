<?php
/**
 * Created by PhpStorm.
 * User: djamaliahamada
 * Date: 07/06/2018
 * Time: 09:39
 */

namespace App\Controller;

use App\common\Helper\UnirestHelper;
use App\Form\LogInType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Unirest\Request as RestRequest;

class IndexController extends BaseController {

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request) {
        $form = $this->createForm(LogInType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid() && $request->isMethod('POST')){
            $datas = $form->getData();
            $body = RestRequest\Body::multipart($datas);
            $response = RestRequest::post('http://localhost:8001/user/auth', UnirestHelper::UNIREST_HEADER_JSON, $body);

            if(in_array($response->code, UnirestHelper::RESPONSE_OK)){
                $this->setSessionAction($datas['email'], $request);
                $this->addFlash("success", "Bienvenue sur notre magnifique site.");
                return $this->render('index/index.html.twig', [
                    'logIn_form' => $form->createView()
                ]);
            }
            else{
                $this->addFlash("danger", "Erreur sur le mot de passe ou l'identifiant.");
                return $this->render('index/index.html.twig', [
                    'logIn_form' => $form->createView()
                ]);
            }
        }

        return $this->render('index/index.html.twig', [
            'logIn_form' => $form->createView()
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request) {
        $this->destroySession();

        $this->addFlash("success", "Déconnexion bien effectué.");
        return $this->redirectToRoute('index');
    }

    private function setSessionAction($email, Request $request){
        $response = RestRequest::get('http://localhost:8001/users/search?email=' . $email);
        $user = json_decode($response->raw_body, true)['content']['users'][0];

        $this->addToSession('email', $user['email']);
        $this->addToSession('username', $user['artist_name']);
        $this->addToSession('userId', $user['id']);
        $this->addToSession('userRole', $user['role']['id']);
    }

    /**
     * @Route("/profile")
     */
    public function accueil() {
        return $this->render('user/profile.html.twig');
    }

}