<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 14/06/18
 * Time: 10:29
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController {

    protected function addToSession($key, $val) {
        $this->get('session')->set($key, $val);
    }

    protected function getFromSession($key) {
        return $this->get('session')->get($key);
    }

    protected function destroySession() {
        return $this->get('session')->invalidate();
    }

}