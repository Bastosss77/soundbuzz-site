<?php
/**
 * Created by PhpStorm.
 * User: djamaliahamada
 * Date: 06/06/2018
 * Time: 17:04
 */

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class LogInType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->setMethod('POST')
            ->add('email',EmailType::class)
            ->add('password',PasswordType::class)
            ->add('submit', SubmitType::class,  array('label' => 'Se connecter'));
    }

    public function getBlockPrefix() {
        return 'form';
    }
}