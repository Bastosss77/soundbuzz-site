<?php
/**
 * Created by PhpStorm.
 * User: bastos
 * Date: 02/07/18
 * Time: 10:42
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AddTrackType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title',TextType::class)
            ->add('composer', TextType::class)
            ->add('musicType', ChoiceType::class, array('choices' => array("" => 0), 'multiple' => false))
            ->add('description', TextareaType::class)
            ->add('explicit_content', CheckboxType::class, array('required' => false))
            ->add('downloadable', CheckboxType::class, array('required' => false))
            ->add('visible', CheckboxType::class, array('required' => false))
            ->add('duration', HiddenType::class)
            ->add('photo', HiddenType::class)
            ->add('filename', HiddenType::class)
            ->setMethod('POST');
    }

    public function getBlockPrefix() {
        return 'form';
    }
}