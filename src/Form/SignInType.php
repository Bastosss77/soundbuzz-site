<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SignInType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->setMethod("POST")
            ->add('lastname',TextType::class)
            ->add('firstname',TextType::class)
            ->add('email',EmailType::class)
            ->add('age',IntegerType::class, array( 'attr' => array('min' => '1', 'max' => '150')))
            ->add('password',PasswordType::class)
            ->add('password_verify',PasswordType::class)
            ->add('artist_name',TextType::class)
            ->add('submit', SubmitType::class,  array('label' => 'Inscription'));

    }

    public function getBlockPrefix() {
        return "form";
    }
}
