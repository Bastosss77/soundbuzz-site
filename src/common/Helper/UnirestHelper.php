<?php

namespace App\common\Helper;

class UnirestHelper
{
    const RESPONSE_OK = array(200, 201);
    const RESPONSE_KO = array('audio/mpeg', 'audio/ogg', 'audio/flac', 'audio/x-flac', 'audio/x-wav');

    const UNIREST_HEADER_JSON = array('Accept' => 'application/json');
}